var numList = 0;
var numListCreated = 0;

$(document).ready(function() {
	$('#addList').attr('disabled', false);
	$('#addList').focus();
});


$.fn.setCursorToTextEnd = function() {
    var $initialVal = this.val();
    this.val($initialVal);
};


$('#addList').click(function() {
	if(numList < 4) {
		var listElement = $('#hidden-list').children();
		$('#board').append(listElement.clone());
		posChildren = numList + 1;
		$('#board').children(':nth-child('+posChildren+')').children().children(':nth-child('+1+')').children().focus();
		numList++;
		if(numList >= 4)
			$('#addList').attr('disabled', true);
	}
	else
		$('#addList').attr('disabled', true);
});

function deleteTask(obj) {
	if($(obj).hasClass('close')) 
		var task = $(obj).parent();
	else
		var task = $(obj).parent().parent();
	task.parent().children('.add-task').removeClass('hidden');
	task.parent().children('.add-task').focus();
	task.remove();
}

$('#board').on('click', '.save-list', function() {
	var list = $(this).parent().parent();
	var name = list.children().children('.name-list').val();
	if((name != null) && (name != "") ) {
		list.children().remove();	//Delete the List's content
		var buttonNewTask = $('#hidden-task-button').children().clone();
		list.append("<h4 class='title-list'>"+name+ "</h4> <div class='list-tasks'></div>");
		list.children('.list-tasks').append(buttonNewTask);
		list.children('.list-tasks').children().focus();
		numListCreated++;
	}
});

function deleteList(obj) {
	$(obj).parent().parent().parent().remove();
	numList--;
	numListCreated--;
	if(numList < 4)
		$('#addList').attr('disabled', false);
}

$('#board').on('click', '.add-task', function() {
	var taskElement = $('#hidden-task').children();
	$(this).addClass('hidden');
	$(this).parent().prepend(taskElement.clone());
	$(this).parent().children().children(':nth-child('+1+')').children().focus();
});

$('#board').on('click', '.save-task', function() {
	var task = $(this).parent().parent();
	var name = task.children().children('.name-task').val();
	if((name != null) && (name != "") ) {
		task.children().remove();	//Delete the Task's content
		task.append(
			" <button type='button' class='close' onClick='deleteTask(this);'>"
			+"	<span>×</span>"
			+"</button>"
			+"<p class='task-name'>"+name+"</p>"
		);
		task.append($('#hidden-task-action').children().clone());
		task.parent().children('.add-task').removeClass('hidden');
		task.parent().children('.add-task').focus();
	}
});

function right(obj) {
	task = $(obj).parent().parent();
	list = task.parent().parent().parent();
	if( list.index()<numListCreated-1) {
		nroList = list.index() + 2;
		list.parent().children(':nth-child('+nroList+')').children().children('.list-tasks').prepend(task.clone());
		task.remove();
	}
}

function left(obj) {
	task = $(obj).parent().parent();
	list = task.parent().parent().parent();
	nroList = list.index();
	if(nroList>0) {
		list.parent().children(':nth-child('+nroList+')').children().children('.list-tasks').prepend(task.clone());
		task.remove();	
	}
}

$('#board').on('dblclick', '.title-list', function() {
	var title = $(this).text();
	var listElement = $('#hidden-list-edit').children().children().children();
	$(this).parent().prepend(listElement.clone());
	$(this).parent().children(':nth-child(1)').children().val(title);
	numListCreated--;
	$(this).remove();
})

$('#board').on('click', '.save-edit-list', function() {
	var list = $(this).parent().parent();
	var name = list.children().children('.name-list').val();
	if((name != null) || (name != "") ) {
		list.children(':nth-child(1)').remove();
		list.children(':nth-child(1)').remove();
		list.prepend("<h4 class='title-list'>"+name+ "</h4> <div class='list-tasks'></div>");
		list.children('.list-tasks').children().focus();
		numListCreated++;
	}
});

$('#board').on('dblclick', '.task-name', function() {
	var name = $(this).text();
	var taskElement = $('#hidden-task').children();
	$(this).parent().parent().prepend(taskElement.clone());
	$(this).parent().parent().children().children(':nth-child('+1+')').children().val(name).focus();
	$(this).parent().remove();
})